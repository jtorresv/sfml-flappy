#include "GameState.hpp"
#include "GameOverState.hpp"

namespace Winterpixel
{
	GameState::GameState(GameDataRef data) :_data(data)
	{
		
	}

	void GameState::Init()
	{
		if (!_hitSoundBuffer.loadFromFile(HIT_SOUND_FILEPATH))
		{
			std::cout << "Error loading hit sound effect!" << std::endl;
		}
		if (!_wingSoundBuffer.loadFromFile(WING_SOUND_FILEPATH))
		{
			std::cout << "Error loading wing sound effect!" << std::endl;
		}
		if (!_pointSoundBuffer.loadFromFile(POINT_SOUND_FILEPATH))
		{
			std::cout << "Error loading point sound effect!" << std::endl;
		}

		_hitSound.setBuffer(_hitSoundBuffer);
		_wingSound.setBuffer(_wingSoundBuffer);
		_pointSound.setBuffer(_pointSoundBuffer);

		std::cout << "Game State" << std::endl;

		// Load various textures
		_data->assets.LoadTexture("Pipe Up", PIPE_UP_FILEPATH);
		_data->assets.LoadTexture("Pipe Down", PIPE_DOWN_FILEPATH);
		_data->assets.LoadTexture("Land", LAND_FILEPATH);
		_data->assets.LoadTexture("Bird Frame 1", BIRD_FRAME_1_FILEPATH);
		_data->assets.LoadTexture("Bird Frame 2", BIRD_FRAME_2_FILEPATH);
		_data->assets.LoadTexture("Bird Frame 3", BIRD_FRAME_3_FILEPATH);
		_data->assets.LoadTexture("Bird Frame 4", BIRD_FRAME_4_FILEPATH);
		_data->assets.LoadTexture("Scoring Pipe", SCORING_PIPE_FILEPATH);
		_data->assets.LoadFont("Flappy Font", FLAPPY_FONT_FILEPATH);

		// Construct game objects
		pipe = new Pipe(_data);
		land = new Land(_data);
		bird = new Bird(_data);
		flash = new Flash(_data);
		hud = new HUD(_data);

		// Load game bg
		_data->assets.LoadTexture("Game Background", GAME_BACKGROUND_FILEPATH);
		_background.setTexture(this->_data->assets.GetTexture("Game Background"));

		_score = 0;
		hud->UpdateScore(_score);

		_gameState = GameStates::eReady;
	}

	void GameState::HandleInput()
	{
		sf::Event event;

		while (_data->window.pollEvent(event))
		{
			// If the window is closed
			if (sf::Event::Closed == event.type)
			{
				_data->window.close();
			}

			// Checking if screen is clicked
			if (_data->input.IsSpriteClicked(_background, sf::Mouse::Left, _data->window))
			{
				if (GameStates::eGameOver != _gameState)
				{
					_gameState = GameStates::ePlaying;
					bird->Tap();

					_wingSound.play();
				}
			}
		}
	}

	void GameState::Update(float delta)
	{
		/* Game logic below */

		// Run basic logic when it isnt game over
		if (GameStates::eGameOver != _gameState)
		{
			// Run animations
			bird->Animate(delta);

			land->MoveLand(delta);
		}

		// Check time passed while playing
		if (GameStates::ePlaying == _gameState)
		{
			pipe->MovePipes(delta);

			if (clock.getElapsedTime().asSeconds() > PIPE_SPAWN_FREQUENCY)
			{
				// Random y offset for pipes
				pipe->RandomisePipeOffset();

				// Spawn another set of pipes
				pipe->SpawnInvisiblePipe();
				pipe->SpawnBottomPipe();
				pipe->SpawnTopPipe();
				pipe->SpawnScoringPipe();

				// Reset the clock
				clock.restart();
			}

			// Update the state of the bird
			bird->Update(delta);

			// Get the landsprites dynamically
			std::vector<sf::Sprite> landSprites = land->GetSprites();

			// Check if bird collided with land sprites
			for (int i = 0; i < landSprites.size(); i++)
			{
				if (collision.CheckSpriteCollision(bird->GetSprite(), 0.7f, landSprites.at(i), 1.0f))
				{
					_gameState = GameStates::eGameOver;
					clock.restart();

					_hitSound.play();
				}
			}

			// Get the pipe sprites dynamically
			std::vector<sf::Sprite> pipeSprites = pipe->GetSprites();

			// Check if bird collided with pipe sprites
			for (int i = 0; i < pipeSprites.size(); i++)
			{
				if (collision.CheckSpriteCollision(bird->GetSprite(), 0.625f, pipeSprites.at(i), 1.0f))
				{
					_gameState = GameStates::eGameOver;
					clock.restart();

					_hitSound.play();
				}
			}

			// Update the score while game is active
			if (GameStates::ePlaying == _gameState)
			{
				std::vector<sf::Sprite> &scoringSprites = pipe->GetScoringSprites();

				for (int i = 0; i < scoringSprites.size(); i++)
				{
					if (collision.CheckSpriteCollision(bird->GetSprite(), 0.625f, scoringSprites.at(i), 1.0f))
					{
						_score++;

						hud->UpdateScore(_score);

						std::cout << "Score: " << _score << std::endl;

						scoringSprites.erase(scoringSprites.begin() + i);

						_pointSound.play();
					}
				}
			}
		}

		// Check if game state is game over
		if (GameStates::eGameOver == _gameState)
		{
			flash->Show(delta);

			if (clock.getElapsedTime().asSeconds() > TIME_BEFORE_GAME_OVER_APPEARS)
			{
				_data->machine.AddState(StateRef(new GameOverState(_data, _score)), true);
			}
		}
	}
		

	void GameState::Draw(float delta)
	{
		// Clear the window
		_data->window.clear();

		// Draw and display the background, game objects
		_data->window.draw(_background);
		pipe->DrawPipes();
		land->DrawLand();
		bird->Draw();
		flash->Draw();
		hud->Draw();
		_data->window.display();
	}
}