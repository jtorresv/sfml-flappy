#include "InputManager.hpp"

namespace Winterpixel
{
	InputManager::InputManager()
	{
	}

	InputManager::~InputManager()
	{
	}

	bool InputManager::IsSpriteClicked(sf::Sprite object, sf::Mouse::Button button, sf::RenderWindow & window)
	{
		// Detect mouse button press
		if (sf::Mouse::isButtonPressed(button))
		{
			// Create a temporary rectangle
			sf::IntRect tempRect(object.getPosition().x, object.getPosition().y, object.getGlobalBounds().width, object.getGlobalBounds().height);

			// Check for collision
			if (tempRect.contains(sf::Mouse::getPosition(window)))
			{
				return true;
			}
		}

		return false;
	}

	sf::Vector2i InputManager::GetMousePosition(sf::RenderWindow & window)
	{
		return sf::Mouse::getPosition(window);
	}
}