#pragma once

#include <SFML\Graphics.hpp>
#include <vector>

#include "Game.hpp"

namespace Winterpixel
{
	class Land
	{
	public:
		Land(GameDataRef data);

		void MoveLand(float delta);
		void DrawLand();

		const std::vector<sf::Sprite> &GetSprites() const;

	private:
		GameDataRef _data;
		std::vector<sf::Sprite> _landSprites;
	};
}