#include "SplashState.hpp"
#include "MainMenuState.hpp"

namespace Winterpixel
{
	SplashState::SplashState(GameDataRef data) :_data(data)
	{
	}

	void SplashState::Init()
	{
		_data->assets.LoadTexture("Splash State Background", SPLASH_SCENE_BACKGROUND_FILEPATH);
		_background.setTexture(this->_data->assets.GetTexture("Splash State Background"));
	}

	void SplashState::HandleInput()
	{
		sf::Event event;

		while (_data->window.pollEvent(event))
		{
			// If the window is closed
			if (sf::Event::Closed == event.type)
			{
				_data->window.close();
			}
		}
	}

	void SplashState::Update(float delta)
	{
		if (_clock.getElapsedTime().asSeconds() > SPLASH_STATE_SHOW_TIME)
		{
			// Switch to the main menu
			_data->machine.AddState(StateRef(new MainMenuState(_data)), true);
		}
	}

	void SplashState::Draw(float delta)
	{
		// Clear the window
		_data->window.clear();

		// Draw and display the background
		_data->window.draw(_background);
		_data->window.display();
	}
}