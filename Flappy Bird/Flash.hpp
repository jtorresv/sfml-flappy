#pragma once

#include <SFML\Graphics.hpp>

#include "Game.hpp"
#include "Definitions.hpp"

namespace Winterpixel
{

	class Flash
	{
	public:
		Flash(GameDataRef data);
		void Show(float delta);
		void Draw();

	private:
		GameDataRef _data;
		sf::RectangleShape _shape;
		bool _flashOn;
	};

}