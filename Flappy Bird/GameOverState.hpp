#pragma once

#include <SFML\Graphics.hpp>
#include <iostream>
#include <sstream>

#include "State.hpp"
#include "Game.hpp"
#include "Definitions.hpp"

namespace Winterpixel
{
	class GameOverState : public State
	{
	public:
		GameOverState(GameDataRef data, int score);

		void Init();
		void HandleInput();
		void Update(float delta);
		void Draw(float delta);

	private:
		GameDataRef _data;
		sf::Sprite _background;
		sf::Sprite _gameOverTitle;
		sf::Sprite _gameOverContainer;
		sf::Sprite _retryButton;
		sf::Sprite _medal;
		sf::Text _scoreText;
		sf::Text _highScoreText;
		int _score;
		int _highScore;
	};
}