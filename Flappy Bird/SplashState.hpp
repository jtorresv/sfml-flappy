#pragma once

#include <SFML\Graphics.hpp>
#include <iostream>
#include <sstream>

#include "State.hpp"
#include "Game.hpp"
#include "Definitions.hpp"

namespace Winterpixel
{
	class SplashState : public State
	{
	public:
		SplashState(GameDataRef data);

		void Init();
		void HandleInput();
		void Update(float delta);
		void Draw(float delta);

	private:
		GameDataRef _data;
		sf::Clock _clock;
		sf::Sprite _background;
	};
}