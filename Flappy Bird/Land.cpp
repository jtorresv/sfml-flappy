#include "Land.hpp"
#include "Definitions.hpp"

namespace Winterpixel
{
	Land::Land(GameDataRef data) : _data(data)
	{
		// Init sprites
		sf::Sprite sprite(_data->assets.GetTexture("Land"));
		sf::Sprite sprite2(_data->assets.GetTexture("Land"));

		// Positiion ground sprites
		sprite.setPosition(0, _data->window.getSize().y - sprite.getGlobalBounds().height);
		sprite2.setPosition(sprite.getGlobalBounds().width, _data->window.getSize().y - sprite.getGlobalBounds().height);

		// Add to sprites vector
		_landSprites.push_back(sprite);
		_landSprites.push_back(sprite2);
	}

	void Land::MoveLand(float delta)
	{
		for (unsigned short int i = 0; i < _landSprites.size(); i++)
		{
			// Calculate the movement
			float movement = PIPE_MOVEMENT_SPEED * delta;

			// Move to the left
			_landSprites.at(i).move(-movement, 0.0f);

			// Remove it when it goes off screen
			if (_landSprites.at(i).getPosition().x < 0 - _landSprites.at(i).getGlobalBounds().width)
			{
				sf::Vector2f position(_data->window.getSize().x, _landSprites.at(i).getPosition().y);
				_landSprites.at(i).setPosition(position);
			}
		}
	}

	void Land::DrawLand()
	{
		for (unsigned short int i = 0; i < _landSprites.size(); i++)
		{
			// Draw the land
			_data->window.draw(_landSprites.at(i));
		}
	}

	const std::vector<sf::Sprite>& Land::GetSprites() const
	{
		return _landSprites;
	}
}