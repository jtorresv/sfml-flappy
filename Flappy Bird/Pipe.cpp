#include "Pipe.hpp"
#include <iostream>
#include <random>
#include <functional>
#include <memory>

namespace Winterpixel
{
	Pipe::Pipe(GameDataRef data) : _data(data) 
	{
		_landHeight = _data->assets.GetTexture("Land").getSize().y;
		_pipeSpawnYOffset = 0;
	}

	void Pipe::SpawnBottomPipe()
	{
		// Init sprite
		sf::Sprite sprite(_data->assets.GetTexture("Pipe Up"));

		// Position sprite
		sprite.setPosition(_data->window.getSize().x, _data->window.getSize().y - sprite.getGlobalBounds().height - _pipeSpawnYOffset);

		// Add to vector of pipes
		pipeSprites.push_back(sprite);
	}

	void Pipe::SpawnTopPipe()
	{
		// Init sprite
		sf::Sprite sprite(_data->assets.GetTexture("Pipe Down"));

		// Position sprite
		sprite.setPosition(_data->window.getSize().x, - _pipeSpawnYOffset);

		// Add to vector of pipes
		pipeSprites.push_back(sprite);
	}

	void Pipe::SpawnInvisiblePipe()
	{
		// Init sprite
		sf::Sprite sprite(_data->assets.GetTexture("Pipe Up"));

		// Position sprite
		sprite.setPosition(_data->window.getSize().x, _data->window.getSize().y - sprite.getGlobalBounds().height);

		// Make the pipe invisible
		sprite.setColor(sf::Color(0, 0, 0, 0));

		// Add to vector of pipes
		pipeSprites.push_back(sprite);
	}

	void Pipe::SpawnScoringPipe()
	{
		// Init sprite
		sf::Sprite sprite(_data->assets.GetTexture("Scoring Pipe"));

		// Position sprite
		sprite.setPosition(_data->window.getSize().x, 0);

		// Add to vector of pipes
		scoringPipes.push_back(sprite);
	}

	void Pipe::MovePipes(float delta)
	{
		// Iterate over pipes
		for (unsigned short int i = 0; i < pipeSprites.size(); i++)
		{
			// Check if pipes go off screen
			if (pipeSprites.at(i).getPosition().x < 0 - pipeSprites.at(i).getGlobalBounds().width)
			{
				// Remove them to manage memory
				pipeSprites.erase(pipeSprites.begin() + i);
			}
			else
			{
				// Calculate the movement
				float movement = PIPE_MOVEMENT_SPEED * delta;

				// Move pipe to the left
				pipeSprites.at(i).move(-movement, 0);
			}
		}

		//std::cout << pipeSprites.size() << std::endl;

		// Iterate over scoring pipes
		for (unsigned short int i = 0; i < scoringPipes.size(); i++)
		{
			// Check if pipes go off screen
			if (scoringPipes.at(i).getPosition().x < 0 - scoringPipes.at(i).getGlobalBounds().width)
			{
				// Remove them to manage memory
				scoringPipes.erase(scoringPipes.begin() + i);
			}
			else
			{
				// Calculate the movement
				float movement = PIPE_MOVEMENT_SPEED * delta;

				// Move pipe to the left
				scoringPipes.at(i).move(-movement, 0);
			}
		}
	}

	void Pipe::DrawPipes()
	{
		for (unsigned short int i = 0; i < pipeSprites.size(); i++)
		{
			_data->window.draw(pipeSprites.at(i));
		}
	}

	void Pipe::RandomisePipeOffset()
	{
		// Generate random numbers using C++11 random library
		std::random_device rd;
		std::mt19937 mt(rd());
		std::uniform_real_distribution<double> dist(0, _landHeight);
		_pipeSpawnYOffset = (int)dist(mt);
	}

	const std::vector<sf::Sprite>& Pipe::GetSprites() const
	{
		return pipeSprites;
	}

	std::vector<sf::Sprite>& Pipe::GetScoringSprites() 
	{
		return scoringPipes;
	}
}