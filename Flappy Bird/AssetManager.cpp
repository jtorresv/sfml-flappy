#pragma once

#include "AssetManager.hpp"

namespace Winterpixel
{
	void AssetManager::LoadTexture(std::string name, std::string fileName)
	{
		sf::Texture texture;

		// On successful load, add texture to map
		if (texture.loadFromFile(fileName))
		{
			this->_textures[name] = texture;
		} 
	}

	sf::Texture & AssetManager::GetTexture(std::string name)
	{
		return this->_textures.at(name);
	}

	void AssetManager::LoadFont(std::string name, std::string fileName)
	{
		sf::Font font;

		// On successful load, add font to map
		if (font.loadFromFile(fileName))
		{
			this->_fonts[name] = font;
		}
	}

	sf::Font & AssetManager::GetFont(std::string name)
	{
		return this->_fonts.at(name);
	}
}