#include "Game.hpp"
#include "SplashState.hpp"

namespace Winterpixel
{
	Game::Game(int width, int height, std::string title)
	{
		_data->window.create(sf::VideoMode(width, height), title, sf::Style::Close | sf::Style::Titlebar);

		// Default game state
		_data->machine.AddState(StateRef(new SplashState(this->_data)));
		
		this->Run();
	}

	Game::~Game()
	{
	}

	void Game::Run()
	{
		float newTime;
		float frameTime;
		float interpolation;

		float currentTime = this->_clock.getElapsedTime().asSeconds();
		float accumulator = 0.0f;

		// The game loop!
		while (this->_data->window.isOpen())
		{
			// Process any changes in the game states
			this->_data->machine.ProcessStateChanges();

			newTime = this->_clock.getElapsedTime().asSeconds();

			// Get the time between each frame
			frameTime = newTime - currentTime;

			// Restrict it so it doesn't go too high
			if (frameTime > 0.25f)
			{
				frameTime = 0.25f;
			}

			// Get the current time
			currentTime = newTime;
			accumulator += frameTime;

			// Game gets updated 
			while (accumulator >= dt)
			{
				this->_data->machine.GetActiveState()->HandleInput();
				this->_data->machine.GetActiveState()->Update(dt);
				accumulator -= dt;
			}

			// Rendering is getting updated
			interpolation = accumulator / dt;
			this->_data->machine.GetActiveState()->Draw(interpolation);
		}
	}
}
