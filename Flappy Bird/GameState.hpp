#pragma once

#include <SFML\Graphics.hpp>
#include <SFML\Audio.hpp>
#include <iostream>
#include <sstream>

#include "State.hpp"
#include "Game.hpp"
#include "Definitions.hpp"
#include "Pipe.hpp"
#include "Land.hpp"
#include "Bird.hpp"
#include "Collision.hpp"
#include "Flash.hpp"
#include "HUD.hpp"

namespace Winterpixel
{
	class GameState : public State
	{
	public:
		GameState(GameDataRef data);

		void Init();
		void HandleInput();
		void Update(float delta);
		void Draw(float delta);

	private:
		GameDataRef _data;
		sf::Sprite _background;
		sf::Clock clock;
		Pipe *pipe; 
		Land *land; 
		Bird *bird; 
		Flash *flash;
		HUD *hud;
		Collision collision;
		int _gameState;
		int _score;
		sf::SoundBuffer _hitSoundBuffer;
		sf::SoundBuffer _wingSoundBuffer;
		sf::SoundBuffer _pointSoundBuffer;
		sf::Sound _hitSound;
		sf::Sound _wingSound;
		sf::Sound _pointSound;
	};
}