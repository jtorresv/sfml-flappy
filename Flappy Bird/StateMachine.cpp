#include "StateMachine.hpp"

namespace Winterpixel
{
	void StateMachine::AddState(StateRef newState, bool isReplacing)
	{
		this->_isAdding = true;
		this->_isReplacing = isReplacing;
		this->_newState = std::move(newState);
	}

	void StateMachine::RemoveState()
	{
		this->_isRemoving = true;
	}

	void StateMachine::ProcessStateChanges()
	{
		// If removing a state and there are states present
		if (this->_isRemoving && !this->_states.empty())
		{
			// Remove the state
			this->_states.pop();

			// Check if states are empty
			if (!this->_states.empty())
			{
				// Resume the next state (top state)
				this->_states.top()->Resume();
			}

			this->_isRemoving = false;
		}

		// If there is a state being added
		if (this->_isAdding)
		{
			// If it's not empty
			if (!this->_states.empty())
			{
				// If a state is being replaced
				if (this->_isReplacing)
				{
					// Remove the state
					this->_states.pop();
				}
				else
				{
					// Pause it, don't get rid of it
					this->_states.top()->Pause();
				}
			}
			// Run the new state
			this->_states.push(std::move(this->_newState));
			this->_states.top()->Init();
			this->_isAdding = false;
		}
	}

	StateRef &StateMachine::GetActiveState()
	{
		// Top is the active state
		return this->_states.top();
	}
}
